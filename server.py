#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time

#Se pasa el puerto al que ha de escuchar como parámetro al programa
PORT = 5060


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    dicc = {} #Diccionario vacío en el que se van a incluir posteriormente los datos


    def jsontoregistered(self): 
        """
        Comprueba si hay un fichero llamado "registered.json" en el diccionario.
        """
        try:
            with open ('registered.json', 'r') as jsonfile:
                self.dicc = json.load(jsonfile)
                self.expired()
        except(NameError, FileNotFoundError):
            pass


    def registered2json(self): 
        """
        Imprime en el fichero "registered.json". Escribe diccionario en "json".
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.dicc, jsonfile, indent=4)

    
         

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n") #Línea tras petición del cliente
        ip = self.client_address[0]
        port = self.client_address[1]
        line = self.rfile.read()
        new_line = line.decode('utf-8').split(" ")[0]

        if len(self.dicc) == 0:
            self.jsontoregistered

        if new_line[0] == 'REGISTER':
            address = str(ip) + ":" + str(port)
            user = line.decode('utf-8').split(':')[1].split("")[0] 
            expires_value = line.decode('utf-8').split("\r\n")[1].split("")[1]

            """
            Tiempo de expiración es tiempo actual más valor de expiración
            """
            tiempo_actual = time.time()
            tiempo_expires = tiempo_actual + int(expires_value) 

            """
            Conversión de los tiempos a "string"
            """
            tiempo_expires_str = time.strftime('%Y-%m-%d %H:%M:%S',
                                             time.gmtime(tiempo_expires))
            tiempo_actual_str = time.strftime('%Y-%m-%d %H:%M:%S',
                                            time.gmtime(tiempo_actual))
           
            self.dicc[user] = {'address: ' + address, 'expires: ' + tiempo_expires_str}
            
            if int(expires_value) == 0:
                print("Eliminando usuario: " + user + str(self.dicc[user]))

            deleted_user = []
            for lista in self.dicc:
                if tiempo_actual_str >= self.dicc[user]['expires: ']:
                    print("Usuario eliminado.")
                    cleardeleted_user.append(lista)
        
            """
            Lista con usuarios eliminados que se elimina del diccionario
            """                    
            for usuario in deleted_user: 
                del self.dicc[usuario]
            print("OK")
            self.registered2json()




def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Lanzando servidor UDP de eco ({PORT})...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
