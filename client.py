#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():

    """
    Parámetros que introduce el cliente
    """
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        LINE = sys.argv[3]
        ADDRESS = sys.argv[4]
        EXPIRES = int(sys.argv[5])
        if LINE == 'REGISTER':
            LINE = LINE.upper()
    except IndexError:
        sys.exit("Usage: python3 client.py <ip> <puerto> register <sip_address> <expires_value>")



    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((IP, PORT))
            SIP = (str(LINE) + ' ' 'sip:' + str(ADDRESS) +
           ' ' + 'SIP/2.0\r\n' + 'Expires: ' + str(EXPIRES) + '\r\n\r\n')
            print("Enviando:", SIP)
            my_socket.send(bytes(SIP, 'utf-8'))
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))
        print("Cliente terminado.")

    except ConnectionRefusedError:
        print("Error conectando a servidor")



if __name__ == "__main__":
    main()
